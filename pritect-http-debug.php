<?php
/*
Plugin Name: Pritect HTTP Debug
Version: 0.0.1
Author Name: James Golovich
Description: Plugin to log HTTP Requests originating from WordPress
License: GPL2
*/
/*  Copyright 2014  James Golovich  (email : james@gnuinter.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

require_once 'src/class-pritect-http-debug.php';
$http_debugger = new Pritect_HTTP_Debug();
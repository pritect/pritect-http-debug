<?php

final class Pritect_HTTP_Debug {
	public function __construct() {
		add_action( 'http_api_debug', array( $this, 'http_api_debug' ), 10, 5 );
		add_filter( 'pritect_http_debug_ignore', array( $this, 'block_local_requests' ), 10, 3 );
	}

	/*
	 * Do we really care to log cron requests?
	 * If so remove this filter or add an overriding filter
	 */
	public function block_local_requests( $default, $url, $args ) {
		$site_url = get_site_url();
		if ( 0 === strpos( $url, $site_url ) ) {
			return true;
		}
		return $default;
	}

	public function create_dir( $dir ) {
		@mkdir( $dir, apply_filters( 'pritect_http_debug_dir_mode', 0700 ), true );
		// @todo Create .htaccess and index.php?
	}

	public function log_filename( $url ) {
		$wp_upload_dir = wp_upload_dir();
		$subdir = '/pritect_http_debug';
		$base_dir = apply_filters( 'pritect_http_debug_base_dir', $wp_upload_dir[ 'basedir' ]  . $subdir);
		$parts = wp_parse_url( $url );
		$log_dir = $base_dir . '/' . $parts[ 'host' ];
		$this->create_dir( $log_dir );
		$file = sanitize_file_name( $parts[ 'path' ] );
		if ( strlen( $file ) <= 1 ) {
			$file = 'index';
		}
		return $log_dir . '/' . $file . '_' . time();

	}

	public function http_api_debug( $response = array(), $context, $class, $args = array(), $url ) {
		if ( true !== apply_filters( 'pritect_http_debug_ignore', false, $url, $args ) ) {
			$request = array(
				'url'      => $url,
				'class'    => $class,
				'context'  => $context,
				'args'     => $args,
				'response' => $response,
			);
			if ( function_exists( 'debug_backtrace' ) ) {
				$request[ 'backtrace' ] = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS );
			}
			$filename = $this->log_filename( $url );
			if ( !empty( $filename ) ) {
				$fp = fopen( $filename, 'w' );
				fwrite( $fp, json_encode( $request ) );
				fclose( $fp );
			}
		}
	}
}